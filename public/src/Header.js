import React from 'react'

export default function Header({starTrekCharacter}) {
  return (<h1>{`Rock/Paper/Scissors/Lizard/${starTrekCharacter}`}</h1>)
}
