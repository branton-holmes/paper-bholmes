/**
 * Created by Branton on 6/12/2017.
 */
import { ImmutableStore } from 'fluxible-immutable-utils';
import Actions from '../static/Actions.js';

function defaultState() {
    return {
        cpu: 0,
        you: 0,
        tie: 0
    }
}

export default class GameStore extends ImmutableStore {
    static storeName = 'GameStore';

    static handlers = {
        [Actions.ATTACK_CLICKED]: 'handleAttackClicked'
    };

    constructor(dispatcher) {
        super(dispatcher);

        this.setState(defaultState());
    }

    handleAttackClicked(payload) {
        let value;
        switch (payload) {
            case 'you':
                value = this.getYourWins();
                this.setState({you: value + 1});
                break;
            case 'cpu':
                value = this.getCPUWins();
                this.setState({cpu: value + 1});
                break;
            case 'tie':
                value = this.getTies();
                this.setState({tie: value + 1});
                break;
        }
    }

    getCPUWins() {
        return this.get('cpu')
    }

    getYourWins() {
        return this.get('you')
    }

    getTies() {
        return this.get('tie')
    }
}