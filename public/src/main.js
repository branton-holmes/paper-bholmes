import React from 'react'
import ReactDOM from 'react-dom'

import Header from './Header.js'
import Welcome from './Welcome.js'
import Game from './Game.js'

const famousVulcan = 'Spock';
const rootElement = document.createElement('div');
document.body.appendChild(rootElement);

ReactDOM.render(
  <div className="welcome">
    <Header starTrekCharacter={famousVulcan}/>
    <Welcome email="scott.morley@insidesales.com"/>
      <Game/>
  </div>,
  rootElement
);
