/**
 * Created by Branton on 6/12/2017.
 */
import React from 'react'
import {connectToStores} from 'fluxible-addons-react'
import Counter from './Counter'
import ButtonGroup from './ButtonGroup'
import {makeAttack} from './actions/GameActionCreators'

@connectToStores(['GameStore'], (context) => {
    const GameStore = context.getStore('GameStore');

    return {
        cpu: GameStore.getCPUWins(),
        you: GameStore.getYourWins(),
        ties: GameStore.getTies()
    };
})

export default class Game extends React.Component {
    static propTypes = {
        cpu: React.PropTypes.Number,
        you: React.PropTypes.Number,
        ties: React.PropTypes.Number

    };

    static contextTypes = {
        executeAction: React.PropTypes.func
    };

    attackSelected = option => {
        this.context.executeAction(makeAttack, option)
    };

    render() {
        const {cpu, you, tie} = this.state;

        const options = ['rock','paper','scissors','lizard','spock'];

        return (
            <div>
                <h1>Play Rock, Paper, Scissors, Lizard, Spock</h1>
                <div className="flexRow">
                    <Counter title='CPU' count={cpu}/>
                    <Counter title='You' count={you}/>
                    <Counter title='ties' count={tie}/>
                </div>
                <ButtonGroup items={options} buttonClicked={this.attackSelected}/>
            </div>
        )
    }
}