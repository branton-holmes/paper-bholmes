/**
 * Created by Branton on 6/12/2017.
 **/
import React from 'react'

export default class ButtonGroup extends React.Component {

    render() {
        const {items, buttonClicked} = this.props;

        const buttons = items.map((item) => {
            return <button key={item} onClick={buttonClicked}>{item}'</button>
        });

        return (
            <div className='flexRow'>{buttons}</div>
        )
    }
}