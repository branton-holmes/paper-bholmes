import React from 'react'

export default class Welcome extends React.Component {
  render() {
    return (
      <div>
        <h2>Technical Challenge</h2>

        <h3>Implement Rock/Paper/Scissors/Lizard/Spock</h3>
        <p>
          Use the rules below to implement Rock/Paper/Scissors/Lizard/Spock Game.
          It should keep track of how many times the player has won and how many times the server has won.
        </p>

        <h3>Running the back end skeleton service</h3>
        <p>
          You probably already have that going since you're reading this page.
          Feel free to modify the layout and add routes as you see fit.
        </p>

        <h3>Game Rules/Requirements</h3>
        <p>
          The game is an expansion on the classic game of <strong>Rock, Paper, Scissors</strong>.
          Each player picks a variable and reveals it at the same time. The winner is the one who defeats the others.
          In a tie, the process is repeated until a winner is found.
        </p>

        <ul>
          <li>Scissors cuts Paper</li>
          <li>Paper covers Rock</li>
          <li>Rock crushes Lizard</li>
          <li>Lizard poisons Spock</li>
          <li>Spock smashes Scissors</li>
          <li>Scissors decapitates Lizard</li>
          <li>Lizard eats Paper</li>
          <li>Paper disproves Spock</li>
          <li>Spock vaporizes Rock</li>
          <li>and finally Rock blunts Scissors</li>
        </ul>

        <h4>Good Luck!</h4>
      </div>
    )
  }
}
