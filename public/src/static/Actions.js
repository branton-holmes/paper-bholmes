/**
 * Created by Branton on 6/12/2017.
 */
import keyMirror from 'keymirror';

const Actions = keyMirror({
    ATTACK_CLICKED: null,
});

export default Actions;
