/**
 * Created by Branton on 6/12/2017.
 */
import Actions from '../static/Actions.js'

export async function makeAttack(context, {option}) {
    const playerChoice = option.target.textContent;
    const randomNumber = Math.floor(Math.random() * (5));
    const CPUChoices = ['rock','spock','paper','lizard','scissors'];
    const CPUChoice = CPUChoices[randomNumber];
    const map = {};

    CPUChoices.forEach(function(choice, i) {
        map[choice] = {};
        for (let j = 0, half = (CPUChoices.length - 1) / 2; j < CPUChoices.length; j++) {
            const opposition = (i+j)%CPUChoices.length;
            if (!j) {
                map[choice][choice] = 'tie';
            } else if  (j <= half) {
                map[choice][CPUChoices[opposition]] = CPUChoices[opposition];
            } else {
                map[choice][CPUChoices[opposition]] = choice;
            }
        }
    });

    function compare(player, cpu) {
        const winner = (map[player])[cpu];
        if (winner === 'tie') {
            return 'tie';
        } else if (winner === playerChoice) {
            return 'you';
        } else {
            return 'cpu'
        }
    }

    context.dispatch(Actions.ATTACK_CLICKED, compare(playerChoice, CPUChoice));
}