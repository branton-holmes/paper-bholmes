/**
 * Created by Branton on 6/12/2017.
 */
import React from 'react'

export default class Counter extends React.Component {
    render() {
        const {title, count} = this.props;

        return (
            <h2>{title} : {count}</h2>
        )
    }
}